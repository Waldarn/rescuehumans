Instructions :

- Create database with sql request in folder titled "DB",

- In the "backend" module (folders src/main/resources), update keys :
"spring.datasource.password" and "spring.datasource.username" in the
"application.properties" file,

- Launch main class "paloit.Main" with config SpringBoot in the "backend" folder,

- Execute command "ng-serve" in the "frontend" folder to launch app angular
