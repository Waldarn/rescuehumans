export interface Patient {

  firstName: string;
  lastName: string;
  illness: number;
  levelPain: number;
  hospital: number;

}
