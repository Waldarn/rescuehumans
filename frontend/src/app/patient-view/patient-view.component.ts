import { Component, OnInit } from '@angular/core';
import {PatientService} from "../services/patient.service";
import {Subscription} from "rxjs";
import {DataService} from "../services/data.service";

@Component({
  selector: 'app-patient-view',
  templateUrl: './patient-view.component.html',
  styleUrls: ['./patient-view.component.css']
})
export class PatientViewComponent implements OnInit {

  isAuth = false;
  lastUpdate = new Date();
  mobile = false;

  patients: any[] | undefined;
  patientSubscription : Subscription | undefined;

  constructor(private patientService: PatientService, private dataService: DataService) {

  }

  ngOnInit(){
    if (window.screen.width < 420) { // Manage mobile version
      this.mobile = true;
    } else {
      this.mobile = false;
    }

    this.onFetch();
    this.patientSubscription = this.patientService.patientsSubject.subscribe(
      (patients: any[]) => {
        this.patients = patients;
      }
    );
    this.patientService.emitPatientSubject();
  }

  onFetch() {
    this.patientService.getPatientFromServer();
  }

}
