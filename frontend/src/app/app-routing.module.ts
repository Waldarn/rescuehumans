import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {EditInfoComponent} from './edit-info/edit-info.component';
import {PatientComponent} from './patient/patient.component';
import {PatientViewComponent} from './patient-view/patient-view.component';
import {AuthComponent} from './auth/auth.component';
import {SinglePatientComponent} from './single-patient/single-patient.component';
import {ErrorPageComponent} from './error-page/error-page.component';
import {AuthGuard} from './services/auth-guard-service';

const routes: Routes = [

{
  path: '',
  component: AuthComponent
},
{
  path: 'auth',
  component: AuthComponent
},
{
  path: 'onlive',
  component: PatientViewComponent
},
{
  path: 'onlive/:id',
  component: SinglePatientComponent
},
{
  path: 'book',
  component: EditInfoComponent
},
{
  path: 'not-found',
  component: ErrorPageComponent
},
{
  path: '**',
  redirectTo: 'not-found'
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
