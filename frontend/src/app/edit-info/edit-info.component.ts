import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms";
import {Router} from "@angular/router";

import { Illness } from './hardData/illness';
import { Hospital } from './hardData/hospital';
import {PatientService} from "../services/patient.service";
import {DataService} from "../services/data.service"
import {Patient} from "../core/model/patient";


@Component({
  selector: 'app-edit-info',
  templateUrl: './edit-info.component.html',
  styleUrls: ['./edit-info.component.css']
})
export class EditInfoComponent implements OnInit {

  selectedIllness?: Illness;
  selectedHospital?: Hospital;
  patient: Patient;
  stepForm = 0;
  pain = 0;

  constructor(private patientService: PatientService, private dataService: DataService) {
    this.patient = {
      firstName : "",
      lastName : "",
      illness : -1,
      levelPain: -1,
      hospital: -1
    }
  }

  illnesses = this.dataService.getIllnessesFromAPI();
  hospitals = this.dataService.getHospitalFromAPI();

  ngOnInit() {
  }

  onSelect(illness: Illness): void {
    this.selectedIllness = illness;
  }

  onSubmit(form: NgForm) { //Submission form
    const firstName = form.value['firstName'];
    const lastName = form.value['lastName'];
    const illness = form.value['illness'];
    const levelPain = form.value['levelPain'];
    const hospital = form.value['hospital'];

    //Save new patient in dataBase
    this.patientService.addPatientToServer(firstName, lastName, illness, levelPain, hospital);
  }

  getDisplay (action: boolean){ //Next or previous step in form to hide or show step
    if (action){
      this.stepForm=this.stepForm+1;
    } else {
      this.stepForm=this.stepForm-1;
    }
  }

  classHospital(pain: number) { //Sort of hospitals with the level of pain in parameter
      this.hospitals.sort(function(a, b) {
        return a.waitingList[pain].realTime - b.waitingList[pain].realTime;
      });
    this.getDisplay(true); //next step in form
    this.pain = pain;
    console.log('Sort of Hospitals');
  }

  transform(value: number): string {
    const hours = Math.floor(value/60);
    const minutesLeft = value % 60;
    return `${hours > 0 ? hours+'h' : ''}${minutesLeft < 10 ? '0': ''}${minutesLeft}min`
  }


}
