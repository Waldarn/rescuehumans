import { Illness } from './illness';

export const ILLNESSES: Illness[] = [
  { id: 11, name: 'Mortal Cold' },
  { id: 12, name: 'Happy Euphoria' },
  { id: 13, name: 'Withering Parasite' },
  { id: 14, name: 'Deaths Delusions' },
  { id: 15, name: 'Intense Intolerance' },
  { id: 16, name: 'Whispering Hepatitis' },
  { id: 17, name: 'Dynama' },
  { id: 18, name: 'Dr IQ' },
  { id: 19, name: 'Magma' },
  { id: 20, name: 'Tornado' }
];
