import { Hospital } from './hospital';

export const HOSPITALS: Hospital[] = [
  { id: 11, name: 'Saint Joseph' },
  { id: 12, name: 'Remi' },
  { id: 13, name: 'Watson' },
  { id: 14, name: 'ZombieSafe' }
];
