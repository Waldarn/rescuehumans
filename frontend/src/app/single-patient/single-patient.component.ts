import { Component, OnInit } from '@angular/core';
import {PatientService} from "../services/patient.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-single-patient',
  templateUrl: './single-patient.component.html',
  styleUrls: ['./single-patient.component.css']
})
export class SinglePatientComponent implements OnInit {

  name: string = 'Name';
  illness: string = 'Illness';
  levelPain: number = 0;
  hospital: string = 'Hospital';
  idPatient: number = -1;

  constructor(private patientService: PatientService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    const id = this.route.snapshot.params['id'];
    this.name = this.patientService.getPatientById(+id).First_name +" "+ this.patientService.getPatientById(+id).Last_name ;
    this.illness = this.patientService.getPatientById(+id).Illness;
    this.levelPain = this.patientService.getPatientById(+id).Level_pain;
    this.hospital = this.patientService.getPatientById(+id).Hospital;
    this.idPatient = id;
  }

}
