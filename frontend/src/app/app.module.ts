import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { EditInfoComponent } from './edit-info/edit-info.component';
import { PatientComponent } from './patient/patient.component';
import { PatientViewComponent } from './patient-view/patient-view.component';

// service
import { PatientService} from './services/patient.service';
import { AuthService } from './services/auth.service';
import { AuthComponent } from './auth/auth.component';
import { SinglePatientComponent } from './single-patient/single-patient.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { HttpClientModule } from '@angular/common/http';
import { DataService } from "./services/data.service";
import { NavbarComponent } from './navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    EditInfoComponent,
    PatientComponent,
    PatientViewComponent,
    AuthComponent,
    SinglePatientComponent,
    ErrorPageComponent,
    NavbarComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    PatientService,
    DataService,
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
