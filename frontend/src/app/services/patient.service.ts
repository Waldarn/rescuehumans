import {Subject, Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Illness} from "../core/model/illness";
import {Router} from "@angular/router";

@Injectable()
export class PatientService {

  patientsSubject = new Subject<any[]>();
  private patients: any[] | undefined;
  hospital: any[] = [];

  constructor(private httpClient: HttpClient, private router: Router) {
  }

  emitPatientSubject() {
    // @ts-ignore
    this.patientsSubject.next(this.patients.slice());
  }

  getPatientById(id: number){

    // @ts-ignore
    const patient = this.patients.find(
      (patientObject) => {
        return patientObject.Id_patient === id;
      }
    );
    return patient;
  }

  addPatientToServer(firstName: string, lastName: string, illness: string, levelPain: number, hospital: string){

    let params = new HttpParams();
    params = params.append('First_name', firstName);
    params = params.append('Last_name', lastName);
    params = params.append('Illness', illness);
    params = params.append('Level_pain', levelPain.toString());
    params = params.append('Hospital', hospital);

    this.httpClient
      .post('http://localhost:8080/paloit/demo/add', null, { params : params})
      .subscribe(
        () => {
          console.log('Data recorded');
        },
        (error) => {
          console.log('Save Error : ' + error);
        }
      );

    setTimeout(() => {this.router.navigate(['/onlive']);}, 1000);
    this.emitPatientSubject();
  }

  getPatientFromServer(){
    this.httpClient
    .get<any[]>('http://localhost:8080/paloit/demo/all')
      .subscribe(
        (response) => {
          this.patients = response;
          this.emitPatientSubject();
        },
        (error) => {
          console.log('Loading Error : ' + error);
        }
      );
  }

}
