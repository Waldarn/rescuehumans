import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import { Illness } from '../core/model/illness';
import {Hospital} from "../core/model/hospital";


@Injectable()
export class DataService {

  constructor(private httpClient_ill: HttpClient, private httpClient_hos: HttpClient) {
  }

  private tabIll: any[] = [];
  private tabHos: any[] = [];
  private request: any;

  getIllnessesFromAPI(){

    let illnesses: any[] = [];

    this.httpClient_ill
      .get('http://dmmw-api.australiaeast.cloudapp.azure.com:8080/illnesses')
      .subscribe(
        (response) => {
          (this.request) = response;
          this.tabIll = this.request._embedded.illnesses;

          for (let i = 0; i < this.tabIll.length; i++) {
            illnesses.splice(i, 0, new Illness(this.tabIll[i].illness.name, this.tabIll[i].illness.id))
          }
        },
        (error) => {
          console.log('Loading Error : ' + error);
        }
      );

    return illnesses;
  }

  getHospitalFromAPI(){ // Get data about hospital from api

    let hospitals: any[] = [];

    this.httpClient_hos
      .get('http://dmmw-api.australiaeast.cloudapp.azure.com:8080/hospitals')
      .subscribe(
        (response) => {
          (this.request) = response;
          this.tabHos = this.request._embedded.hospitals;

          for (let i = 0; i < this.tabHos.length; i++) {
            let hos = new Hospital(this.tabHos[i].name, this.tabHos[i].id, this.tabHos[i].waitingList);
            for (let j = 0; j < 5; j++) { //// Store in waitingList a new parameter realTime
              let realTime = hos.waitingList[j].patientCount * hos.waitingList[j].averageProcessTime;
              hos.waitingList[j].realTime = realTime;
            }
            hospitals.splice(i, 0, hos);
          }
        },
        (error) => {
          console.log('Loading Error : ' + error);
        }
      );

    return hospitals;
  }

}
