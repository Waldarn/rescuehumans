import { Component, OnInit, Input } from '@angular/core';
import {PatientViewComponent} from "../patient-view/patient-view.component";
//import {PatientService} from "../services/patient.service";
//import {Patient} from "../core/model/patient";

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css']
})
export class PatientComponent {

  //patients : any;

  @Input() indexOfPatient : number | undefined;
  @Input() patientFirstName: string | undefined;
  @Input() patientLastName: string | undefined;
  @Input() patientIllness: string | undefined;
  @Input() patientLevelPain: number | undefined;
  @Input() patientHospital: string | undefined;
  @Input() id : number | undefined;

  constructor(private pv: PatientViewComponent) {

  }

  mobile = this.pv.mobile; //mobile version

}
