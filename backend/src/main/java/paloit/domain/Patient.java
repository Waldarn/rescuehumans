package paloit.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity // This tells Hibernate to make a table out of this class
public class Patient {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY )
    private Integer id_patient;

    private String first_name;

    private String last_name;

    private String illness;

    private Integer level_pain;

    private String hospital;

    public Integer getId_patient() {
        return id_patient;
    }

    public void setId(Integer id_patient) {
        this.id_patient = id_patient;
    }

    public String getFirst_name() { return first_name; }

    public void setFirst_name(String first_name) { this.first_name = first_name; }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String name) {
        this.last_name = name;
    }

    public Integer getLevel_pain() { return level_pain; }

    public void setLevel_pain(Integer level_pain) { this.level_pain = level_pain; }

    public String getIllness() {
        return illness;
    }

    public void setIllness(String illness) {
        this.illness = illness;
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }
}
