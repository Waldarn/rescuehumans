package paloit.repository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

import org.springframework.data.repository.CrudRepository;
import paloit.domain.Patient;

public interface PatientRepository extends CrudRepository<Patient, Integer> {

}
