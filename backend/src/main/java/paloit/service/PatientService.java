package paloit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import paloit.domain.Patient;
import paloit.repository.PatientRepository;

import java.util.List;

@Service
public class PatientService implements IPatientService {
    @Autowired
    private PatientRepository repository;

    @Override
    public List<Patient> findAll() {
        return (List<Patient>) repository.findAll();
    }

}

