package paloit.service;

import paloit.domain.Patient;

import java.util.List;

public interface IPatientService {

    List<Patient> findAll();
}
